# serengeti_caching_server
## Specs
- lang: Kotlin (JDK 11: corretto-11)
- db: Oracle
- spring boot: 2.5.3

## Naming
- check: 캐싱이 되어 있는치 체크
- save: 새로운 데이터를 캐싱

## Api 명세
- local 프로파일에서만 볼 수 있음 변경은 SwaggerConfig에서 가능
- links: http://localhost:8080/swagger-ui/index.html

## Docker
### build
```bash
# how to
docker build -t ${이미지 이름}:${태그} .
# example 
docker build -t cache-server:0.0.2 .
```
### run
```bash
# CLI
docker run --name cache-server -p 8080:8080 --restart unless-stopped -d cache-server:0.0.2
# Docker Compose
docker-compose up -d
```
### save
- image를 압축 파일로 만들 때 사용
```bash
# how to
docker save --output ${압축 파일 이름}.tar ${이미지 이름}:${태그}
# example
docker save --output cache-server.tar cache-server:0.0.2 
```
### load
- tar파일을 docker image로 만들 때 사용
```bash
# how to
docker load --input ${압축된 파일}
# example 
docker load --input cache-server.tar
```

