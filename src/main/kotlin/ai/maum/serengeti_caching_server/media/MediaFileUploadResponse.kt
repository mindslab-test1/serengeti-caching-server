package ai.maum.serengeti_caching_server.media

data class MediaFileUploadResponse(
    val filename: String,
    val fileDownloadUri: String,
    val fileType: String,
    val fileSize: Int
)