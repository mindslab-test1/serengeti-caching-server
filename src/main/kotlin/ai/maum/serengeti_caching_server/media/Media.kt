package ai.maum.serengeti_caching_server.media

import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.io.Resource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.multipart.MultipartFile

@Service
class Media(
    private val restTemplate: RestTemplate,
) {
    @Value("\${media.location}")
    lateinit var mediaLocation: String

    @Value("\${media.download.location}")
    lateinit var mediaDownloadLocation: String

    @Value("\${media.upload.path}")
    lateinit var mediaUploadPath: String


    fun getUrl(path: String): String {
        return "${mediaDownloadLocation}${path}"
    }

    fun uploadToMediaServer(file: MultipartFile): String {

        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA

        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("file", file.resource)
        body.add("uploadDirectory", mediaUploadPath)

        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange(
            "$mediaLocation/media/file:upload",
            HttpMethod.POST,
            requestEntity,
            object : ParameterizedTypeReference<MediaFileUploadResponse?>() {})

        val downloadUri = response.body!!.fileDownloadUri
        return downloadUri.split("/media")[1]
    }

    fun uploadToMediaServer(file: Resource): String {
        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA

        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("file", file)
        body.add("uploadDirectory", mediaUploadPath)

        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange(
            "$mediaLocation/media/file:upload",
            HttpMethod.POST,
            requestEntity,
            object : ParameterizedTypeReference<MediaFileUploadResponse?>() {})

        val downloadUri = response.body!!.fileDownloadUri
        return downloadUri.split("/media")[1]
    }
}