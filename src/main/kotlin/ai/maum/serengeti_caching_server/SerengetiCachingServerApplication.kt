package ai.maum.serengeti_caching_server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SerengetiCachingServerApplication

fun main(args: Array<String>) {
    runApplication<SerengetiCachingServerApplication>(*args)
}
