package ai.maum.serengeti_caching_server.jpa.lipsync

import org.springframework.data.repository.CrudRepository

interface LipSyncCacheRepository : CrudRepository<LipSyncCache, Long> {
    fun findTopByModelAndTextAndWidthAndHeight(
        model: String?,
        text: String?,
        width: Int?,
        height: Int?
    ): LipSyncCache?
}