package ai.maum.serengeti_caching_server.jpa.lipsync

import javax.persistence.*

@Entity
class LipSyncCache {
    @Id
    @SequenceGenerator(
        name = "LIP_SYNC_CACHE_SEQ_GEN",
        sequenceName = "LIP_SYNC_CACHE_SEQ",
        initialValue = 1,
        allocationSize = 1
    )

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LIP_SYNC_CACHE_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null
    var model: String? = null
    var width: Int? = null
    var height: Int? = null
    var text: String? = null
    var path: String? = null
}