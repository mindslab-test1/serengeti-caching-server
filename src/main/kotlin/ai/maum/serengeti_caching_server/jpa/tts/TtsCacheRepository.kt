package ai.maum.serengeti_caching_server.jpa.tts

import org.springframework.data.repository.CrudRepository

interface TtsCacheRepository : CrudRepository<TtsCache, Long> {
    fun findByText(text: String?): TtsCache?
}