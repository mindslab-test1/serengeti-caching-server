package ai.maum.serengeti_caching_server.jpa.tts

import javax.persistence.*

@Entity
class TtsCache {
    @Id
    @SequenceGenerator(
            name = "TTS_CACHE_SEQ_GEN",
            sequenceName = "TTS_CACHE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TTS_CACHE_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null
    var text: String? = null
    var path: String? = null
}