package ai.maum.serengeti_caching_server.http

import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import java.time.Duration

private val CONNECTION_TIMEOUT = 60L;
private val READ_TIMEOUTE = 60L;

@Configuration
class RestTemplateConfig {
    @Bean
    fun restTemplate(): RestTemplate {
        val httpRequestFactory = HttpComponentsClientHttpRequestFactory()
        val httpClient = HttpClientBuilder.create().setMaxConnTotal(200).setMaxConnPerRoute(20).build()
        httpRequestFactory.httpClient = httpClient
        return RestTemplateBuilder()
            .setConnectTimeout(Duration.ofSeconds(CONNECTION_TIMEOUT))
            .setReadTimeout(Duration.ofSeconds(READ_TIMEOUTE))
            .requestFactory { httpRequestFactory }
            .build()
    }
}