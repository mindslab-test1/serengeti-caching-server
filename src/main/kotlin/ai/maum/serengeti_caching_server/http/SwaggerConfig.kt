package ai.maum.serengeti_caching_server.http

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket

private val TITLE = "세렝게티";
private val DESC = "세렝게티 Api spec";
private val VERSION = "1.0";

@Configuration
class SwaggerConfig {

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.OAS_30)
            .select()
            .apis(RequestHandlerSelectors.basePackage("ai.maum.serengeti_caching_server.http"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo());
    }

    fun apiInfo(): ApiInfo? {
        return ApiInfoBuilder()
            .title(TITLE)
            .description(DESC)
            .version(VERSION)
            .build()
    }
}
/**
 * reference:
 * https://bcp0109.tistory.com/326
 */

