package ai.maum.serengeti_caching_server.http.tts.dto

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class TtsSaveReq(
    @field: NotNull
    var text: String? = null,
    @field: NotNull
    var file: MultipartFile? = null
)