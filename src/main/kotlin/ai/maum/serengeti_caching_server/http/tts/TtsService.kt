package ai.maum.serengeti_caching_server.http.tts

import ai.maum.serengeti_caching_server.http.common.dto.MediaPayload
import ai.maum.serengeti_caching_server.http.tts.dto.TtsCheckReq
import ai.maum.serengeti_caching_server.http.tts.dto.TtsSaveReq
import ai.maum.serengeti_caching_server.jpa.tts.TtsCache
import ai.maum.serengeti_caching_server.jpa.tts.TtsCacheRepository
import ai.maum.serengeti_caching_server.media.Media
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class TtsService(
        private val ttsCacheRepository: TtsCacheRepository,
        private val media: Media,
) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    fun runCheck(request: TtsCheckReq): MediaPayload {
        logger.info("run: check")
        val data: TtsCache? = ttsCacheRepository.findByText(text = request.text)

        val response = MediaPayload()
        if (data == null) return response

        response.exist = true
        response.url = media.getUrl(data.path.toString())
        return response
    }

    fun runSave(request: TtsSaveReq): MediaPayload {
        logger.info("run: save")

        val data: TtsCache? = ttsCacheRepository.findByText(text = request.text)
        val mediaPath = media.uploadToMediaServer(request.file!!)

        if(data == null) {
            val ttsCache = TtsCache()
            ttsCache.text = request.text
            ttsCacheRepository.save(ttsCache)
        }
        else {
            data.path = mediaPath
            ttsCacheRepository.save(data)
        }

        val response = MediaPayload()
        response.exist = true
        response.url = media.getUrl(mediaPath)
        return response
    }
}