package ai.maum.serengeti_caching_server.http.tts

import ai.maum.serengeti_caching_server.http.common.dto.BaseRes
import ai.maum.serengeti_caching_server.http.common.dto.MediaPayload
import ai.maum.serengeti_caching_server.http.tts.dto.TtsCheckReq
import ai.maum.serengeti_caching_server.http.tts.dto.TtsSaveReq
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Transactional
@RestController
@RequestMapping("/tts")
class TtsController(
    private val ttsService: TtsService
) {
    @PostMapping(
        "/check",
        consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE),
        produces = arrayOf(MediaType.APPLICATION_JSON_VALUE)
    )
    fun lipSyncCheck(@RequestBody request: TtsCheckReq): ResponseEntity<BaseRes> {
        val payload: MediaPayload = ttsService.runCheck(request)

        val body = BaseRes()
        body.payload = payload
        return ResponseEntity.ok().body(body)
    }

    @PostMapping(
        "/save",
        consumes = arrayOf(MediaType.MULTIPART_FORM_DATA_VALUE),
        produces = arrayOf(MediaType.APPLICATION_JSON_VALUE)
    )
    fun lipSyncSave(request: TtsSaveReq): ResponseEntity<BaseRes> {
        val payload: MediaPayload = ttsService.runSave(request)

        val body = BaseRes()
        body.payload = payload
        return ResponseEntity.ok().body(body)
    }
}