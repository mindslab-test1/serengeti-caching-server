package ai.maum.serengeti_caching_server.http.tts.dto

import javax.validation.constraints.NotNull

data class TtsCheckReq(
    @field: NotNull
    var text: String? = null,
)