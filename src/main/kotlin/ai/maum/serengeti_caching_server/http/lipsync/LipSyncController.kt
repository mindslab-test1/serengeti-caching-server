package ai.maum.serengeti_caching_server.http.lipsync

import ai.maum.serengeti_caching_server.http.common.dto.BaseRes
import ai.maum.serengeti_caching_server.http.common.dto.MediaPayload
import ai.maum.serengeti_caching_server.http.lipsync.dto.LipSyncCheckReq
import ai.maum.serengeti_caching_server.http.lipsync.dto.LipSyncSaveReq
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Transactional
@RestController
@RequestMapping("/lipsync")
class LipSyncController(
    private val lipSyncService: LipSyncService,
) {

    @PostMapping(
        "/check",
        consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE),
        produces = arrayOf(MediaType.APPLICATION_JSON_VALUE)
    )
    fun lipSyncCheck(@Valid @RequestBody request: LipSyncCheckReq): ResponseEntity<BaseRes> {
        val payload: MediaPayload = lipSyncService.runCheck(request)

        val body = BaseRes()
        body.payload = payload
        return ResponseEntity.ok().body(body)
    }

    @PostMapping(
        "/save",
        consumes = arrayOf(MediaType.MULTIPART_FORM_DATA_VALUE),
        produces = arrayOf(MediaType.APPLICATION_JSON_VALUE)
    )
    fun lipSyncSave(@Valid request: LipSyncSaveReq): ResponseEntity<BaseRes> {
        val payload: MediaPayload = lipSyncService.runSave(request)

        val body = BaseRes()
        body.payload = payload
        return ResponseEntity.ok().body(body)
    }
}