package ai.maum.serengeti_caching_server.http.lipsync

import ai.maum.serengeti_caching_server.http.common.dto.MediaPayload
import ai.maum.serengeti_caching_server.http.lipsync.dto.LipSyncCheckReq
import ai.maum.serengeti_caching_server.http.lipsync.dto.LipSyncSaveReq
import ai.maum.serengeti_caching_server.jpa.lipsync.LipSyncCache
import ai.maum.serengeti_caching_server.jpa.lipsync.LipSyncCacheRepository
import ai.maum.serengeti_caching_server.media.Media
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class LipSyncService(
    private val lipSyncCacheRepository: LipSyncCacheRepository,
    private val media: Media,
) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    fun runCheck(request: LipSyncCheckReq): MediaPayload {
        logger.info("run: check")
        val data: LipSyncCache? = lipSyncCacheRepository.findTopByModelAndTextAndWidthAndHeight(
            model = request.model,
            text = request.text,
            width = request.width,
            height = request.height
        )

        val response = MediaPayload()
        if (data == null) return response

        response.exist = true
        response.url = media.getUrl(data.path.toString())
        return response
    }

    fun runSave(request: LipSyncSaveReq): MediaPayload {
        logger.info("run: save")
        val data: LipSyncCache? = lipSyncCacheRepository.findTopByModelAndTextAndWidthAndHeight(
            model = request.model,
            text = request.text,
            width = request.width?.toInt(),
            height = request.height?.toInt()
        )
        val mediaPath = media.uploadToMediaServer(request.file!!)

        if(data == null) {
            val lipSyncCache = LipSyncCache()
            lipSyncCache.model = request.model
            lipSyncCache.text = request.text
            lipSyncCache.width = request.width?.toInt()
            lipSyncCache.height = request.height?.toInt()
            lipSyncCache.path = mediaPath
            lipSyncCacheRepository.save(lipSyncCache)
        }else {
            data.path = mediaPath
            lipSyncCacheRepository.save(data)
        }

        val response = MediaPayload()
        response.exist = true
        response.url = media.getUrl(mediaPath)
        return response
    }
}

