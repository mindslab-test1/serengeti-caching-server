package ai.maum.serengeti_caching_server.http.lipsync.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

data class LipSyncCheckReq(
    @field:NotNull
    @field:NotBlank
    var model: String? = null,
    @field:NotNull
    @field:NotBlank
    var text: String? = null,
    @field:NotNull
    @field:Positive(message = "must be positive number")
    var width: Int? = null,
    @field:NotNull
    @field:Positive(message = "must be positive number")
    var height: Int? = null
)