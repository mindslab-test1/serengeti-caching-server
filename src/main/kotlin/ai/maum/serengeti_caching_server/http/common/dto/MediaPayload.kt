package ai.maum.serengeti_caching_server.http.common.dto

data class MediaPayload(
    var exist: Boolean = false,
    var url: String? = null
)
