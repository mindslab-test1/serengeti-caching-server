package ai.maum.serengeti_caching_server.http.common.config

object StatusCode {
    // 200
    const val SUCCESS: Int = 2000

    // 400
    const val BAD_REQUEST: Int = 4000
    const val UNSUPPORTED_MEDIA_TYPE: Int = 4015
    // 500

    const val UNKNOWN: Int = 9999
}