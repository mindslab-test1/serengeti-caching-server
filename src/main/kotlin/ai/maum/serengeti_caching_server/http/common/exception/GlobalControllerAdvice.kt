package ai.maum.serengeti_caching_server.http.common.exception

import ai.maum.serengeti_caching_server.http.common.config.StatusCode
import ai.maum.serengeti_caching_server.http.common.dto.BaseRes
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindException
import org.springframework.web.HttpMediaTypeException
import org.springframework.web.HttpMediaTypeNotSupportedException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import javax.servlet.http.HttpServletRequest

@RestControllerAdvice
class GlobalControllerAdvice {

    @ExceptionHandler(value = [RuntimeException::class])
    fun exception(e: RuntimeException, request: MethodArgumentNotValidException): ResponseEntity<BaseRes> {
        val message = e.message

        val body = BaseRes()
        body.status = StatusCode.UNKNOWN
        body.message = "$message"
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body)
    }

    @ExceptionHandler(value = [MethodArgumentNotValidException::class, BindException::class])
    fun bindException(
        e: BindException,
        request: HttpServletRequest
    ): ResponseEntity<BaseRes> {
        val target = e.fieldError?.field
        val message = e.fieldError?.defaultMessage

        val body = BaseRes()
        body.status = StatusCode.BAD_REQUEST
        body.message = "$target $message"
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body)
    }

    @ExceptionHandler(value = [HttpMediaTypeNotSupportedException::class])
    fun httpMediaTypeNotSupportedException(
        e: HttpMediaTypeException,
        request: HttpServletRequest
    ): ResponseEntity<BaseRes> {
        val body = BaseRes()
        body.status = StatusCode.UNSUPPORTED_MEDIA_TYPE
        body.message = e.message
        return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(body);
    }
}