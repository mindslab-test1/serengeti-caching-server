package ai.maum.serengeti_caching_server.http.common.dto

import ai.maum.serengeti_caching_server.http.common.config.StatusCode

data class BaseRes(
    var status: Int = StatusCode.SUCCESS,
    var message: String? = null,
    var payload: Any? = null
)