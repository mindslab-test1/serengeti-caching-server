FROM adoptopenjdk/openjdk11:latest

WORKDIR /app

COPY /build/libs/serengeti_caching_server-0.0.1-SNAPSHOT.jar ./application.jar

EXPOSE 8080
CMD ["java", "-jar", "application.jar", "--spring.profiles.active=prod"]